import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-convocatorias',
  templateUrl: './convocatorias.page.html',
  styleUrls: ['./convocatorias.page.scss'],
})
export class ConvocatoriasPage implements OnInit {
  convocatoriaList: any;
  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getConvocatoria();
  }
  getConvocatoria() {
    console.log(this.route.snapshot.paramMap.get('id'));
    this.http.post('http://www.yoloiztac.com.mx/yolo/yaakun/getConvocatoriaC.php', {

      id_categoria:  this.route.snapshot.paramMap.get('id'),
    }).subscribe(
        (data) => {
          console.log(data);
          this.convocatoriaList = data;
        }
    );
  }


}
