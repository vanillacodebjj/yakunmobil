import {Component, OnInit} from '@angular/core';
import * as firebase from 'firebase';
import {ImagePicker} from '@ionic-native/image-picker/ngx';
import {LoadingController, ToastController} from '@ionic/angular';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {Crop} from '@ionic-native/crop/ngx';

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.page.html',
    styleUrls: ['./perfil.page.scss'],
})
export class PerfilPage implements OnInit {
    user: any;

    constructor(private imagePicker: ImagePicker, public toastCtrl: ToastController, private webview: WebView,
                private loadingCtrl: LoadingController,
                private cropService: Crop) {
        this.user = JSON.parse(localStorage.getItem('user'));
        this.user = Array.of(this.user);
    }

    ngOnInit() {
    }

    async uploadImage(imageURI) {
        imageURI = this.webview.convertFileSrc(imageURI);
        const loading = await this.loadingCtrl.create({});
        const toast = await this.toastCtrl.create({
            message: 'Image was updated successfully',
            duration: 3000
        });

        const user = firebase.auth().currentUser;

        const storageRef = firebase.storage().ref(user.uid + '/profilePicture/');
        const filename = Math.floor(Date.now() / 1000);
        // let imageRef = storageRef.child('img');
        const imageRef = storageRef.child(`${filename}.jpg`);

        this.encodeImageUri(imageURI, (image64) => {
            imageRef.putString(image64, 'data_url')
                .then((savedPicture) => {

                    imageRef.getDownloadURL().then((url) => {
                        // Insert url into an <img> tag to "download"
                        console.log(url);
                        loading.present();
                        toast.present();
                        user.updateProfile({
                            photoURL: url
                        }).then(async () => {
                            // Update successful.
                            await loading.dismiss();

                        }).catch((error) => {
                            // An error happened.
                            console.log(error);
                        });

                    }).catch((error) => {
                        console.log(error);
                    });
                }, err => {
                    console.log(err);
                });


        });


    }

    encodeImageUri(imageUri, callback) {
        const c = document.createElement('canvas');
        const ctx = c.getContext('2d');
        const img = new Image();
        img.onload = function() {
            const aux: any = this;
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            const dataURL = c.toDataURL('image/jpeg');
            callback(dataURL);
        };
        img.src = imageUri;
    }

    openImagePickerCrop() {
        this.imagePicker.hasReadPermission().then(
            (result) => {
                if (result === false) {
                    // no callbacks required as this opens a popup which returns async
                    this.imagePicker.requestReadPermission().then( this.openImagePickerCrop);
                } else if (result === true) {
                    this.imagePicker.getPictures({
                        maximumImagesCount: 1
                    }).then(
                        (results) => {
                            for (let i = 0; i < results.length; i++) {
                                this.cropService.crop(results[i], {quality: 75}).then(
                                    newImage => {
                                        this.uploadImage(newImage);
                                    },
                                    error => console.error('Error cropping image', error)
                                );
                            }
                        }, (err) => console.log(err)
                    );
                }
            }, (err) => {
                console.log(err);
            });
    }
}
