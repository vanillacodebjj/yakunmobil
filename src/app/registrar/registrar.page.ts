import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AngularFireAuth} from '@angular/fire/auth';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-registrar',
    templateUrl: './registrar.page.html',
    styleUrls: ['./registrar.page.scss'],
})
export class RegistrarPage implements OnInit {
    nombre: any;
    apellidos: any;
    correo: any;
    contrasena: any;
    telefono: any;
    direccion: any;
    cp: any;
    estado: any;
    municipio: any;
    diaN: any;
    mesN: any;
    anoN: any;
    escuela: any;
    carrera: any;
    fechaN: any;

    constructor(private http: HttpClient, private fire: AngularFireAuth, private alertCtrl: AlertController) {
    }

    ngOnInit() {
    }

    registrar() {
        /*this.http.post('http://www.yoloiztac.com.mx/yolo/mrpomo/inbox.php', {
            nombre: this.nombre,
            apellidos: this.apellidos,
            correo: this.correo,
            contrasena: this.contrasena,
            telefono: this.telefono,
            direccion: this.direccion,
            cp: this.cp,
            estado: this.estado,
            municipio: this.municipio,
            fechaN: this.fechaN,
            escuela: this.escuela,
            carrera: this.carrera,
            option: 'user',
        }).subscribe(
            (data) => {
                console.log(data);
            });*/
        this.fire.auth.createUserWithEmailAndPassword(this.correo, this.contrasena)
            .then(data => {
                const user = this.fire.auth.currentUser;
                user.sendEmailVerification();
                user.updateProfile({
                    displayName: this.nombre,
                    photoURL: 'assets/imgs/boy.svg'
                }).then( datas => {
                    // Update successful.
                    console.log(datas);

                }).catch(error => {
                    // An error happened.
                    console.log(error);
                });
                this.confirm('Se ha enviado un correo de verificación');
            })
            .catch(error => {
                console.log('got an error ', error);
                this.alert(error.message);
            });
        console.log('Would register user with ', this.correo);
    }
   async alert(message: string) {
      const palert = await this.alertCtrl.create({
            header: 'Info!',
            subHeader: message,
            buttons: ['OK']
        });
      await palert.present();
    }
    async confirm(message: string) {
        const palert = await this.alertCtrl.create({
            header: 'Confirma tu correo',
            subHeader: message,
            buttons: [{
                text: 'Enviar',
                handler: data => {
                    console.log('Send clicked');
                    this.logout();
                }
            }]
        });
        await palert.present();
    }
    logout() {
        localStorage.clear();
        this.fire.auth.signOut();
        location.reload();
    }
}
