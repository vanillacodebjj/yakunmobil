import { Component } from '@angular/core';

import {NavController, NavParams, Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import {AngularFireAuth} from '@angular/fire/auth';
import {HomePage} from './home/home.page';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.scss'],
})
export class AppComponent {
  user: any;
  log: any;
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    /*{
      title: 'List',
      url: '/list',
      icon: 'list'
    },*/
    {
      title: 'Perfil',
      url: '/perfil',
      icon: 'person'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private afAuth: AngularFireAuth,
    private navCtrl: NavController,
    private route: ActivatedRoute, public router: Router
  ) {
    this.initializeApp();
    this.afAuth.authState.subscribe(res => {
      this.user = JSON.parse(localStorage.getItem('user'));
      this.user = Array.of(this.user);
      // console.log(res.emailVerified);
      if (res && res.emailVerified === true) {
        this.log = true;
        console.log('user is logged in');

        this.navCtrl.navigateRoot('/home');
      } else if (res && res.emailVerified === false) {
        // if (val == 'fb') {
        this.log = true;
        console.log('login fb');
        this.navCtrl.navigateRoot('/home');
      } else {
            this.log = false;
            console.log('user not logged in');
            this.navCtrl.navigateForward('/login');
      }
  });
      }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

    logout() {
        localStorage.clear();
        this.afAuth.auth.signOut();
        location.reload();
    }
}
