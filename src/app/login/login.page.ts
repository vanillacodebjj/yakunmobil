import { Component, OnInit } from '@angular/core';
import {AlertController, MenuController, NavController, Platform, ToastController} from '@ionic/angular';
import {AngularFireAuth} from '@angular/fire/auth';
import {Facebook} from '@ionic-native/facebook/ngx';
import {HttpClient} from '@angular/common/http';
import * as firebase from 'firebase/app';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  email: any;
  pass: any;
  user: any = {};
  showUser: boolean = false;
  constructor(public nav: NavController, public forgotCtrl: AlertController, public menu: MenuController, public toastCtrl: ToastController,
              private fire: AngularFireAuth, private alertCtrl: AlertController,
              private facebook: Facebook,
              private afAuth: AngularFireAuth,
              private platform: Platform,
              private http: HttpClient) {
    this.menu.swipeEnable(false);
  }

  ngOnInit() {
  }

  // go to register page
  register() {
    this.nav.navigateForward('/registrar');
  }

  // login and go to home page
  login() {
    this.fire.auth.signInWithEmailAndPassword(this.email , this.pass)
        .then( data => {
          localStorage.setItem('user',  JSON.stringify(data.user));
          console.log('got some data');

          location.reload();

          // user is logged in
        })
        .catch( error => {
          console.log('---------------------------------------------------------');
          console.log( error);
          this.alert(error.message);
        });
    console.log('Would sign in with ', this.email);
  }
  async alert(messages) {
  const alert = await  this.alertCtrl.create({
    header: 'Info!',
    message: messages,
      buttons: ['OK']
    });

  return await alert.present();
  }
  async forgotPass() {
    const forgot = await this.forgotCtrl.create({
      header: '¿Olvidaste tu contraseña?',
      subHeader: 'Ingresa tu correo se enviara un link a tu correo para recuperar tu contraseña.',
      inputs: [
        {
          name: 'email',
          placeholder: 'Correo',
          type: 'email'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Enviar',
          handler: data => {
            console.log('Send clicked');
            this.presentToast();
          }
        }
      ]
    });
    forgot.present();
  }
async presentToast() {
  const toast = await this.toastCtrl.create({
    message: 'El link fue enviado correctamente',
    duration: 3000,
    position: 'top',
    cssClass: 'dark-trans',
    closeButtonText: 'OK',
    showCloseButton: true
  });
  toast.present();
}

// ----------------------------------------- FACEBOOK ----------------------------------------

  nativeLoginFacebook()  {
    try {
      return this.facebook.login(['email', 'public_profile']).then(res => {
        const facebookCredential = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
        return this.afAuth.auth.signInWithCredential(facebookCredential).then(success => {
          console.log(success);
          localStorage.setItem('user',  JSON.stringify(success));
          this.registrerUser( success);
        });
      }).catch((error) => {
        console.log('Error login facebook: ' + error);
        console.log('Error login facebook: ' + JSON.stringify(error));
      });

    } catch (err) {
      console.log(err);

    }
  }

  webLoginFacebook() {
    try {
      return this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(res => {
        console.log(res.user);
        localStorage.setItem('user',  JSON.stringify(res.user));
        this.registrerUser( res.user);
      });

    } catch (err) {
      console.log(err);

    }
  }

  doLogin() {
    if (this.platform.is('cordova')) {
      this.nativeLoginFacebook();
      console.log(this.user);
    } else {
      this.webLoginFacebook();
      console.log(this.user);
    }

  }
  registrerUser(user) {

    this.http.post("http://www.yoloiztac.com.mx/yolo/mrpomo/registrerUsers/addUser.php",{
      email: user.email,
      a_paterno: user.family_name,
      sexo: user.gender,
      nombre: user.displayName,
      profile_picture: user.photoURL,
      opcion: 'addUser',
    }).subscribe( data => {
          console.log(data);
        }

    );
  }
  getInfo() {
    this.facebook.api('/me?fields=id,name,email,first_name,picture,last_name,gender', [ 'public_profile', 'email'])
        .then(data => {
          console.log(data);
          this.showUser = true;
          this.user = data;
        })
        .catch(error => {
          console.error( error );
        });
  }

}
