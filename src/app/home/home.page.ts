import { Component } from '@angular/core';
import {NavController} from '@ionic/angular';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  ListaCategorias: any;
constructor(private navCtrl: NavController, private http: HttpClient) {
  this.getCateforias();
}
  VerConvocatoria(id) {
    this.navCtrl.navigateForward('/convocatorias/' + id);
  }
  getCateforias() {
  this.http.get('http://www.yoloiztac.com.mx/yolo/yaakun/getCategorias.php').subscribe( (data) => {
    console.log(data);
    this.ListaCategorias = data;
  } );
  }
}
